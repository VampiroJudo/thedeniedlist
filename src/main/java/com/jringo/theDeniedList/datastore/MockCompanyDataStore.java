package com.jringo.theDeniedList.datastore;


import com.jringo.theDeniedList.company.CompanyProfile;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Repository
public class MockCompanyDataStore {

    private static final List<CompanyProfile> COMPANY_PROFILES = new ArrayList<>();

    static {
        COMPANY_PROFILES.add(new CompanyProfile(UUID.randomUUID(), "Singularity", "Sub-Branch of NASA", 200 ));
        COMPANY_PROFILES.add(new CompanyProfile(UUID.randomUUID(), "MK Ultra ", "**Restricted**", 75 ));
        COMPANY_PROFILES.add(new CompanyProfile(UUID.randomUUID(), "Recoverable.Inc", "UFO Retrieval Service", 100 ));
        COMPANY_PROFILES.add(new CompanyProfile(UUID.randomUUID(), "Strange Sights", "Avant Garde Travel Company", 37 ));
        COMPANY_PROFILES.add(new CompanyProfile(UUID.randomUUID(), "Synchtronic", "Information Retrieval Service", 23 ));
        COMPANY_PROFILES.add(new CompanyProfile(UUID.randomUUID(), "BlackWater", "Military Contracting Company", 2000 ));
        COMPANY_PROFILES.add(new CompanyProfile(UUID.randomUUID(), "Charlie", "SigInt Analysis Corp.", 170 ));
        COMPANY_PROFILES.add(new CompanyProfile(UUID.randomUUID(), "Beta", "Systems Analysis Corp.", 80 ));
        COMPANY_PROFILES.add(new CompanyProfile(UUID.randomUUID(), "Lindo", "Coffee Company/**Restricted**", 30 ));
    }

    public List<CompanyProfile> getCompanyProfiles() {
        return COMPANY_PROFILES;
    }
}
