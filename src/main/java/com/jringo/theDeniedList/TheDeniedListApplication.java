package com.jringo.theDeniedList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheDeniedListApplication {

	public static void main(String[] args) {
		SpringApplication.run(TheDeniedListApplication.class, args);
	}

}
