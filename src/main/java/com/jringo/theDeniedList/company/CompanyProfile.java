package com.jringo.theDeniedList.company;

import java.util.Objects;
import java.util.UUID;

public class CompanyProfile {

    private UUID companyId;
    private String name;
    private String description;
    private int employeeCount;


    public CompanyProfile(UUID companyId,
                          String name,
                          String description,
                          int employeeCount) {
        this.companyId = companyId;
        this.name = name;
        this.description = description;
        this.employeeCount = employeeCount;
    }

    public void setCompanyId(UUID companyId) {
        this.companyId = companyId;
    }

    public UUID getCompanyId() {
        return companyId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setEmployeeCount(int employeeCount) {
        this.employeeCount = employeeCount;
    }

    public int getEmployeeCount() {
        return employeeCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyProfile that = (CompanyProfile) o;
        return Objects.equals(companyId, that.companyId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(employeeCount, that.employeeCount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyId, name, description, employeeCount);
    }
}