package com.jringo.theDeniedList.company;


import com.jringo.theDeniedList.datastore.MockCompanyDataStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DataAccessService {

    private final MockCompanyDataStore mockCompanyDataStore;

    @Autowired
    public DataAccessService(MockCompanyDataStore mockCompanyDataStore) {
        this.mockCompanyDataStore = mockCompanyDataStore;
    }

    List<CompanyProfile> getCompanyProfiles() {
        return mockCompanyDataStore.getCompanyProfiles();
    }

}

