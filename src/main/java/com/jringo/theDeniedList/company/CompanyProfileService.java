package com.jringo.theDeniedList.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompanyProfileService {

    private final DataAccessService dataAccessService;

    @Autowired
    public CompanyProfileService(DataAccessService dataAccessService) {
        this.dataAccessService = dataAccessService;
    }

    List<CompanyProfile> getCompanyProfiles() {
        return dataAccessService.getCompanyProfiles();
    }
}