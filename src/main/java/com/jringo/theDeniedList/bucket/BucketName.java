package com.jringo.theDeniedList.bucket;

public enum BucketName {

    PROFILE_IMAGE("list-company-bucket");

    private final String bucketName;

    BucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getBucketName() {
        return bucketName;
    }
}
